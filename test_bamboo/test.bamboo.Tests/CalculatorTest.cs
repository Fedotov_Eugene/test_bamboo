﻿using System;
using NUnit.Framework;
using test_bamboo;

namespace test.bamboo.Tests
{
    public class CalculatorTest
    {
        [Test]
        public void Add()
        {
            var calcualtor = new Calculator();

            var a = 1;
            var b = 1;
            var expectedResult = 2;
            var result = calcualtor.Add(1, 1);

            Assert.AreEqual(expectedResult, result);
        }

        [Test]
        public void Minus()
        {
            var calcualtor = new Calculator();

            var a = 1;
            var b = 1;
            var expectedResult = 0;
            var result = calcualtor.Minus(1, 1);

            Assert.AreEqual(expectedResult, result);
        }
    }
}
